## Script to automatically backup sql database and files of moodle to separate folder.
import os, sys, time
time = time.strftime("%d_%m_%Y")
mapToCreate = time + "/"
pathToCreate = "/root/BACKUPS/" + mapToCreate
os.mkdir(pathToCreate, 0755)
fileToSave = time + "_name_of_sql_to_save.sql"
iniCommand = "mysqldump database_name > /path/where/to/save" + mapToCreate
os.system(iniCommand + fileToSave)


p1 = "tar -zcvf /root/BACKUPS/"
p2 = "_moodle.tar.gz /var/www/html/moodle"

os.system(p1 + mapToCreate +  time + p2)
