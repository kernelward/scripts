//Credits: examples from https://github.com/dcuddeback/libusb-rs
extern crate libusb;

use std::time::Duration;
use std::thread;

struct UsbDevice<'a> {
    handle: libusb::DeviceHandle<'a>,
    language: libusb::Language,
    timeout: Duration
}

fn main() {
    let x : i32 = 1;
    while x != 0 {
        list_devices().unwrap();
        thread::sleep(Duration::from_millis(20000));
        check_processes();
        thread::sleep(Duration::from_millis(20000));
    }
    
}

fn check_processes() {
    use std::process::Command;
    
    let processes = ["elasticsearch", "logstash", "kibana", "airodump-ng"];
    let pStart = ["sudo service elasticsearch restart",
                 "sudo service logstash restart",
                 "sudo service kibana restart",
                 "sudo /home/pi/aircrack-ng-1.2-rc4/src/airodump-ng wlan0mon --output-format json -w /opti/wifidata/data"];
    for x in &processes {
        let output = Command::new("pidof")
                    .arg(x)
                    .output()
                    .expect("failed to execute process");
        if output.stdout.is_empty() {
            for x in &pStart {
                let start = Command::new(x);
            }
        }

    }
}


fn list_devices() -> libusb::Result<()> {
    let timeout = Duration::from_secs(1);
    let context = try!(libusb::Context::new());

    for device in try!(context.devices()).iter() {
        let device_desc = match device.device_descriptor() {
            Ok(d) => d,
            Err(_) => continue
        };

        let mut usb_device = {
            match device.open() {
                Ok(h) => {
                    match h.read_languages(timeout) {
                        Ok(l) => {
                            if l.len() > 0 {
                                Some(UsbDevice {
                                    handle: h,
                                    language: l[0],
                                    timeout: timeout
                                })
                            }
                            else {
                                None
                            }
                        },
                        Err(_) => None
                    }
                },
                Err(_) => None
            }
        };
        let vi1: u16= 0x058f;
        if device_desc.vendor_id() == vi1 { //preveri UUID, mountaj glede na UUID in cp gor
            println!("TU KOPIRAJ LOGE NA USB");
        }

        for n in 0..device_desc.num_configurations() {
            let config_desc = match device.config_descriptor(n) {
                Ok(c) => c,
                Err(_) => continue
            };
            for interface in config_desc.interfaces() {
                for interface_desc in interface.descriptors() {//extern crate libusb;

                    //use std::time::Duration;

                    struct UsbDevice<'a> {
                        handle: libusb::DeviceHandle<'a>,
                        language: libusb::Language,
                        timeout: Duration
                    }
                }
            }
        }
    }
    Ok(())
}